import { message } from "antd";
import axios from "axios";
import {
  FC,
  ReactNode,
  createContext,
  useCallback,
  useEffect,
  useState,
} from "react";

interface IThreatDetectionContext {
  imageUrl?: string;
  imageObjectDetails?: any;
  setImageUrl: (value: string) => void;
  clearWorkspace: () => void;
}

interface IThreatDetectionContextProvider {
  children: ReactNode;
}
export const ThreatDetectionContext = createContext<
  IThreatDetectionContext | undefined
>(undefined);

export const ThreatDetectionContextProvider: FC<
  IThreatDetectionContextProvider
> = ({ children }) => {
  const [imageUrl, setImageUrl] = useState<string>();
  const [imageObjectDetails, setImageObjectDetails] = useState<any>();

  const clearWorkspace = () => {
    setImageObjectDetails(undefined);
    setImageUrl(undefined);
  };
  const getImageAnalysisResults = useCallback(async () => {
    try {
      const { REACT_APP_API_ENDPOINT } = process.env;
      const result = await axios.post(REACT_APP_API_ENDPOINT as string, {
        imageUrl,
      });
      if (result.status === 200) {
        if(result?.data?.message){
          message.error(result?.data?.message || "Something went wrong");
          clearWorkspace();
        } else {
          setImageObjectDetails(result?.data);
        }
      } else {
        message.error(result?.data?.message || "Something went wrong");
        clearWorkspace();
      }
    } catch (error) {
      message.error("Error when retrieving image analysis!");
    }
  }, [imageUrl]);

  useEffect(() => {
    if (imageUrl && imageUrl !== "") {
      getImageAnalysisResults();
    }
  }, [getImageAnalysisResults, imageUrl]);
  return (
    <ThreatDetectionContext.Provider
      value={{
        imageUrl,
        imageObjectDetails,
        clearWorkspace,
        setImageUrl,
      }}
    >
      {children}
    </ThreatDetectionContext.Provider>
  );
};
