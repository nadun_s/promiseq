import Header from "./components/header";
import Footer from "./components/footer";
import "./App.css";
import Home from "./pages/home";
import { ThreatDetectionContextProvider } from "./context";

function App() {
  return (
    <div className="h-full">
      <Header />
      <ThreatDetectionContextProvider>
        <Home />
      </ThreatDetectionContextProvider>
      <Footer />
    </div>
  );
}

export default App;
