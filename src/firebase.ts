// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCFvFqSRcXmY2-H7zAqfKn9NcrHp6-gXGI",
  authDomain: "promiseq-a6092.firebaseapp.com",
  projectId: "promiseq-a6092",
  storageBucket: "promiseq-a6092.appspot.com",
  messagingSenderId: "32252281067",
  appId: "1:32252281067:web:7867f223e441854d5afeae"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);
