import { FC, useContext } from "react";
import { FirebaseImageUploader } from "../../components/formElements/fileUploader";
import { ThreatDetectionContext } from "../../context";
import ImageCanvas from "../../components/imageCanvas/ImageCanvas";

const Home: FC = () => {
  const { imageUrl, imageObjectDetails, setImageUrl, clearWorkspace } =
    useContext(ThreatDetectionContext)!;
  return (
    <div className="my-20 flex flex-col justify-center items-center">
      <h2 className="text-gray-900 text-3xl">Threat detect</h2>
      <p className="text-gray-600 my-1">
        Please upload an image to do a quick threat analysis.
      </p>
      <div className="w-8/12">
        <FirebaseImageUploader onUploadComplete={(url) => setImageUrl(url)} />
      </div>
      {imageUrl && (
        <ImageCanvas
          imageUrl={imageUrl}
          imageObjectDetails={imageObjectDetails}
          onCloseModal={clearWorkspace}
        />
      )}
    </div>
  );
};
export default Home;
