import {
  FC,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import Konva from "konva";
import { Stage, Layer, Image } from "react-konva";
import { IShape, Rectangle } from "./Rectangle";
import { Modal, Spin } from "antd";

type ImageCanvasProps = {
  imageUrl: string;
  imageObjectDetails: any;
  onCloseModal: () => void;
};

const ratio = 0.8;

const ImageCanvas: FC<ImageCanvasProps> = ({
  imageUrl,
  imageObjectDetails,
  onCloseModal,
}) => {
  const [boundingBoxes, setBoundingBoxes] = useState<IShape[]>([]);
  const [selectedBox, setSelectedBox] = useState<string | null>(null);
  const stageWidth = window.innerWidth * ratio;
  const stageHeight = window.innerHeight * ratio;
  const stageRef = useRef<any>(null);
  const [scale, setScale] = useState<{ x: number; y: number }>({ x: 1, y: 1 });

  useLayoutEffect(() => {
    if (stageRef.current) {
      const containerWidth = stageRef.current.offsetWidth;
      const containerHeight = stageRef.current.offsetHeight;
      console.log("aaaaa a", containerWidth);
      setScale({
        x: containerWidth / stageWidth,
        y: containerHeight / stageHeight,
      });
    }
  }, [stageWidth, stageHeight]);
  const checkDeselect = (
    e: Konva.KonvaEventObject<TouchEvent | MouseEvent>
  ) => {
    // deselect when clicked on empty area
    const clickedOnEmpty = e.target === e.target.getStage();
    if (clickedOnEmpty) {
      setSelectedBox(null);
    }
  };

  useEffect(() => {
    if (imageObjectDetails) {
      const boxes = imageObjectDetails.metadata.map(
        (item: any, index: number) => {
          const xMin = stageWidth * item.bbox?.xmin;
          const xMax = stageWidth * item.bbox?.xmax;

          const yMin = stageHeight * item.bbox?.ymin;
          const yMax = stageHeight * item.bbox?.ymax;
          const boundingBoxId = `bound_${index}`;
          return {
            id: boundingBoxId,
            x: xMin,
            y: yMin,
            width: xMax - xMin,
            height: yMax - yMin,
          };
        }
      );

      setBoundingBoxes(boxes);
    }
  }, [imageObjectDetails, stageHeight, stageWidth]);

  const renderBoudingBoxes = () => {
    return boundingBoxes.map((item: IShape, i: number) => (
      <Rectangle
        key={i}
        shapeProps={item}
        isSelected={item.id === selectedBox}
        onSelect={() => {
          setSelectedBox(item.id);
        }}
        onChange={(newAttrs) => {
          const rects = boundingBoxes.slice();
          rects[i] = newAttrs;
          setBoundingBoxes(rects);
        }}
      />
    ));
  };
  const image = useMemo(() => {
    const img = new window.Image();
    img.src = imageUrl;
    return img;
  }, [imageUrl]);

  return (
    <Modal
      title="Threat detect analysis"
      centered
      open={imageUrl !== ""}
      onCancel={onCloseModal}
      footer={false}
      width={"90%"}
    >
        <span className="my-1 text-gray-500">Click on a bounding box to resize</span>
      <Spin spinning={!imageObjectDetails}>
        {/* <div ref={stageRef}> */}
          <Stage
            width={stageWidth}
            height={stageHeight}
            onMouseDown={checkDeselect}
            onTouchStart={checkDeselect}
            className="flex justify-center"
          >
            <Layer>
              <Image
                x={0}
                y={0}
                image={image}
                width={stageWidth}
                height={stageHeight}
              />
              {renderBoudingBoxes()}
            </Layer>
          </Stage>
        {/* </div> */}
      </Spin>
    </Modal>
  );
};
export default ImageCanvas;
