import { FC } from "react";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { InboxOutlined } from "@ant-design/icons";
import { message, Upload } from "antd";
import { storage } from "../../firebase";

const { Dragger } = Upload;
export default interface IFile {
  url: string;
  name: string;
}

interface IFirebaseImageUploader {
  onUploadComplete: (url: string) => void;
  onReset?: () => void;
}
export const FirebaseImageUploader: FC<IFirebaseImageUploader> = ({
  onUploadComplete,
}) => {
  const uploadImage = async (options: any) => {
    const { onSuccess, onError, file, onProgress } = options;
    try {
      const storageRef = ref(storage, `/files/${file.name}`);
      // Receives the storage reference and the file to upload.
      const uploadTask = uploadBytesResumable(storageRef, file);
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const percent = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          onProgress({ percent });
        },
        (err) => onError({ err }),
        () => {
          // download url
          getDownloadURL(uploadTask.snapshot.ref).then((url) => {
            message.success("Image uploaded.");
            onUploadComplete(url);
            console.log(url);
            onSuccess("OK")
          });
        }
      );

      onSuccess("Ok");
    } catch (err) {
      onError({ err });
    }
  };
  return (
    <div className="mb-3 py-10 px-4 w-full">
      <Dragger accept="image/*" multiple={false} customRequest={uploadImage} >
        <div className="flex flex-col justify-evenly h-full">
          <div>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">
              Click or drag image to this area to upload
            </p>
          </div>
        </div>
      </Dragger>
    </div>
  );
};
