function Header() {
  return (
    <nav className="bg-gray-800 p-4 fixed w-full top-0 z-50">
      <div className="container mx-auto flex justify-between items-center">
        <div className="text-white text-2xl font-bold">PromiseQ</div>
      </div>
    </nav>
  );
}

export default Header;
